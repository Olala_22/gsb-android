package fr.cned.emdsgil.suividevosfrais.modele;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import fr.cned.emdsgil.suividevosfrais.R;
import fr.cned.emdsgil.suividevosfrais.outils.Global;
import fr.cned.emdsgil.suividevosfrais.outils.Serializer;

class FraisHfAdapter extends BaseAdapter {

	private final ArrayList<FraisHf> lesFrais ; // liste des frais du mois
	private final LayoutInflater inflater ;
	private final int dataKey;
	private final Context context;
    /**
	 * Constructeur de l'adapter pour valoriser les propriétés
     * @param context Accès au contexte de l'application
     * @param lesFrais Liste des frais hors forfait
     */
	public FraisHfAdapter(Context context, ArrayList<FraisHf> lesFrais, int key) {
		this.inflater = LayoutInflater.from(context) ;
		this.lesFrais = lesFrais ;
		this.dataKey = key;
		this.context = context;
    }
	
	/**
	 * retourne le nombre d'éléments de la listview
	 */
	@Override
	public int getCount() {
		return lesFrais.size() ;
	}

	/**
	 * retourne l'item de la listview à un index précis
	 */
	@Override
	public Object getItem(int index) {
		return lesFrais.get(index) ;
	}

	/**
	 * retourne l'index de l'élément actuel
	 */
	@Override
	public long getItemId(int index) {
		return index;
	}

	/**
	 * structure contenant les éléments d'une ligne
	 */
	private class ViewHolder {
		TextView txtListJour ;
		TextView txtListMontant ;
		TextView txtListMotif ;
		ImageButton imageButton;
	}
	
	/**
	 * Affichage dans la liste
	 */
	@Override
	public View getView(final int index, View convertView, ViewGroup parent) {
		ViewHolder holder ;
		if (convertView == null) {
			holder = new ViewHolder() ;
			convertView = inflater.inflate(R.layout.layout_liste, parent, false) ;
			holder.txtListJour = convertView.findViewById(R.id.txtListJour);
			holder.txtListMontant = convertView.findViewById(R.id.txtListMontant);
			holder.txtListMotif = convertView.findViewById(R.id.txtListMotif);
			holder.imageButton = convertView.findViewById(R.id.cmdSuppHf);
			holder.imageButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					supprimerUneLigne(index);
				}
			});
			convertView.setTag(holder) ;
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.txtListJour.setText(new SimpleDateFormat("yyyy-MM-dd").format(lesFrais.get(index).getJour()));
		holder.txtListMontant.setText(String.format(Locale.FRANCE, "%.2f", lesFrais.get(index).getMontant())) ;
		holder.txtListMotif.setText(lesFrais.get(index).getMotif()) ;
		return convertView ;
	}
	private void supprimerUneLigne(int index) {
		if (Global.fraisMoisHashtable.containsKey(this.dataKey)) {
			// creation du mois et de l'annee s'ils n'existent pas déjà
			Global.fraisMoisHashtable.get(this.dataKey).supprFraisHf(index); ;
			//enregistrer les données dnas le fichier
			Serializer.serialize(Global.fraisMoisHashtable, context) ;
			// rafraichi la liste visuelle
			HfRecapActivity activity = (HfRecapActivity) this.context;
			activity.afficheListe();
		}
	}
}
