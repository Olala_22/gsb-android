package fr.cned.emdsgil.suividevosfrais.outils;
/**
 * Created by khalifa on 09/04/2018.
 */

import android.net.Uri;
import android.os.AsyncTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Classe technique de connexion distante HTTP
 */
public class AccesHTTP  extends AsyncTask<String, String, String> {
    private ArrayList<NameValuePair> parametres;
    public AsyncResponse delegate = null;
    private String operation;
    HttpURLConnection conn;
    URL url = null;

    // variables conctantes
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;

    public AccesHTTP(String operation){
        this.operation = operation;
        parametres = new ArrayList<NameValuePair>();
    }

    public void addParam(String nom, String valeur){
        parametres.add(new BasicNameValuePair(nom,valeur));
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            url = new URL("https://gsbziri.000webhostapp.com/login.php");
            //url = new URL("http://www.android-GSB.zir:81/login.php");
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);

            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("POST");

            // setDoInput and setDoOutput method depict handling of both send and receive
            conn.setDoInput(true);
            conn.setDoOutput(true);
                // Append parameters to URL
            Uri.Builder builder = new Uri.Builder()

                    .appendQueryParameter("operation", this.operation)
                    .appendQueryParameter("user", params[0])
                    .appendQueryParameter("psw", params[1]);
            for (NameValuePair param : parametres) {
                builder.appendQueryParameter(param.getName(), param.getValue());
            }
            String query = builder.build().getEncodedQuery();


            // Open connection for sending data
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();
            conn.connect();
            int response_code = conn.getResponseCode();

            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                System.out.println("$$$$$$ received from server " + result.toString());
                // Pass data to onPostExecute method
                return result.toString();

            } else {
                System.out.println("========================");
                return "unsuccessful";
            }

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            conn.disconnect();
            return "exception";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }

}
