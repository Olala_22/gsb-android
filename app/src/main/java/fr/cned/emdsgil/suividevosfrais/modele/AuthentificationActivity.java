package fr.cned.emdsgil.suividevosfrais.modele;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;

import fr.cned.emdsgil.suividevosfrais.R;
import fr.cned.emdsgil.suividevosfrais.outils.AccesHTTP;
import fr.cned.emdsgil.suividevosfrais.outils.AsyncResponse;
import fr.cned.emdsgil.suividevosfrais.outils.Global;
import fr.cned.emdsgil.suividevosfrais.vue.MainActivity;

/**
 * Created by khalifa on 15/03/2018.
 */

public class AuthentificationActivity extends AppCompatActivity implements AsyncResponse {


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    private String user ;
    private String psw ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentification);
        setTitle("GSB : Transfert des données");

        cmdTransfert_clic();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getString(R.string.retour_accueil))) {
            retourActivityPrincipale() ;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Sur le clic du bouton valider : transfert des données apres verification de l'authentification
     */
    private void cmdTransfert_clic() {
        findViewById(R.id.cmdTransferer).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // recuperation du login et du mpd
                findViewById(R.id.cmdTransferer).setEnabled(false);
                sendData();



                }
        }) ;
    }



    /**
     * Retour à l'activité principa
     * le (le menu)
     */
    private void retourActivityPrincipale() {
        Intent intent = new Intent(AuthentificationActivity.this, MainActivity.class) ;
        startActivity(intent) ;
    }
    private void sendData() {
        findViewById(R.id.progessAuth).setVisibility(View.VISIBLE);
        String userName = ((TextView)findViewById(R.id.editTxtUserName)).getText().toString() ;
        String password = ((TextView)findViewById(R.id.editTxtPsw)).getText().toString() ;
        for(Integer key : Global.fraisMoisHashtable.keySet()) {
            FraisMois fm = Global.fraisMoisHashtable.get(key);
            AccesHTTP accesHTTP = new AccesHTTP("enreg");
            AccesHTTP accesHTTPForDel = new AccesHTTP("suppr");
            accesHTTP.delegate = this;
            accesHTTPForDel.delegate = this;
            try {
                accesHTTP.addParam("lesdonnees", fm.getJsonData().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            accesHTTP.execute(userName, password);
        }
    }
    @Override
    public void processFinish(String authReslt){
        findViewById(R.id.cmdTransferer).setEnabled(true);
        findViewById(R.id.progessAuth).setVisibility(View.INVISIBLE);
        if (!"connection successful".equals(authReslt)) {
            findViewById(R.id.authMsg).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.authMsg).setVisibility(View.INVISIBLE);

        }
        findViewById(R.id.cmdTransferer).setEnabled(true);
    }
}
