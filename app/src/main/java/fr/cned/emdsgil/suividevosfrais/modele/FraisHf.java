package fr.cned.emdsgil.suividevosfrais.modele;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Classe métier contenant la description d'un frais hors forfait
 *
 */
public class FraisHf  implements Serializable {

	private final Float montant ;
	private final String motif ;
	private final Date jour ;
	
	public FraisHf(Float montant, String motif, Date jour) {
		this.montant = montant ;
		this.motif = motif ;
		this.jour = jour ;
	}

	public Float getMontant() {
		return montant;
	}

	public String getMotif() {
		return motif;
	}

	public Date getJour() {
		return jour;
	}

}
