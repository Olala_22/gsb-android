package fr.cned.emdsgil.suividevosfrais.modele;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Classe métier contenant les informations des frais d'un mois
 */
public class FraisMois implements Serializable {

    private Integer mois; // mois concerné
    private Integer annee; // année concernée
    private Integer etape; // nombre d'étapes du mois
    private Integer km; // nombre de km du mois
    private Integer nuitee; // nombre de nuitées du mois
    private Integer repas; // nombre de repas du mois
    private final ArrayList<FraisHf> lesFraisHf; // liste des frais hors forfait du mois

    public FraisMois(Integer annee, Integer mois) {
        this.annee = annee;
        this.mois = mois;
        this.etape = 0;
        this.km = 0;
        this.nuitee = 0;
        this.repas = 0;
        lesFraisHf = new ArrayList<>();
    }

    /**
     * Ajout d'un frais hors forfait
     *
     * @param montant Montant en euros du frais hors forfait
     * @param motif Justification du frais hors forfait
     */
    public void addFraisHf(Float montant, String motif, Date jour) {
        lesFraisHf.add(new FraisHf(montant, motif, jour));
    }

    /**
     * Suppression d'un frais hors forfait
     *
     * @param index Indice du frais hors forfait à supprimer
     */
    public void supprFraisHf(int index) {
        Log.i("user", " trying to remove" + index);
        FraisHf removed = lesFraisHf.remove(index);
        if ( removed != null) {
            Log.i("user", "removed frais with motif" + removed.getMotif());
        }
        Log.i("user", " dans le amthod supprFraisHf list size is " + lesFraisHf.size());
    }

    /**
     * Conversion de fraismois au format json
     * @return
     */
    public JSONObject getJsonData() throws JSONException {
        JSONObject data = new JSONObject();
        data.put("annee", annee);
        data.put("mois", mois);
        data.put("etape", etape);
        data.put("km", km);
        data.put("nuitee", nuitee);
        data.put("repas", repas);
        JSONArray arr = new JSONArray();
        for (FraisHf element : lesFraisHf) {
            JSONObject fhf = new JSONObject();
            fhf.put("montant", element.getMontant());
            fhf.put("motif", element.getMotif());
            fhf.put("date", element.getJour());
            arr.put(fhf);
        }
        data.put("fraisHF", arr);
        /*
        for (int i = 0; i< lesFraisHf.size(); i++){
            FraisHf montant = lesFraisHf.get(0);
            FraisHf motif = lesFraisHf.get(1);
            FraisHf date = lesFraisHf.get(2);
            data.put("montant", montant);
            data.put("motif", motif);
            data.put("date", date);
        }*/

        return data;
    }

    public Integer getMois() {
        return mois;
    }

    public void setMois(Integer mois) {
        this.mois = mois;
    }

    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    public Integer getEtape() {
        return etape;
    }

    public void setEtape(Integer etape) {
        this.etape = etape;
    }

    public Integer getKm() {
        return km;
    }

    public void setKm(Integer km) {
        this.km = km;
    }

    public Integer getNuitee() {
        return nuitee;
    }

    public void setNuitee(Integer nuitee) {
        this.nuitee = nuitee;
    }

    public Integer getRepas() {
        return repas;
    }

    public void setRepas(Integer repas) {
        this.repas = repas;
    }

    public ArrayList<FraisHf> getLesFraisHf() {
        return lesFraisHf;
    }

}
