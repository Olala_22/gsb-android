package fr.cned.emdsgil.suividevosfrais.outils;

/**
 * Created by khalifa on 09/04/2018.
 */

public interface AsyncResponse {
    void processFinish(String output);
}
